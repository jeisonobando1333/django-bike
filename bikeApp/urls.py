from django.urls import path
from bikeApp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.base, name='home'),
    path('service/',views.service, name='service'),
    path('blog/', views.blog, name='blog'),
    path('contact/', views.contact, name='contact'),
    path('store/', views.store, name='store')
]
# save images and views themselves
urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
