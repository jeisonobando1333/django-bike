from django.apps import AppConfig


class BikeappConfig(AppConfig):
    name = 'bikeApp'
