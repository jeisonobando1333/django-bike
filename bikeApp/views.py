from django.shortcuts import render

# Create your views here.


def base(request):

    return render(request, "bikeApp/home.html")


def contact(request):

    return render(request, "bikeApp/contact.html")


def blog(request):

    return render(request, "bikeApp/blog.html")


def service(request):

    return render(request, "bikeApp/service.html")


def store(request):

    return render(request, "bikeApp/store.html")
