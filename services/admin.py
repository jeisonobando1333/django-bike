from django.contrib import admin
from .models import Service

# Register your models here.
class ServiceManager(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('title','description','created')
    search_fields = ('title',)
    list_filter = ('created',)
    date_hierarchy = ('created')
admin.site.register(Service,ServiceManager)
